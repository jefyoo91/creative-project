<!DOCTYPE html>
<html>
<head>
	<title>Recipe site login</title>
	<style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	</style>
</head>
<body>
	<div class="box">
	<form action="process_openid.php" method="post">
	<input id="start" name="start" type="hidden" value="true" />
	<fieldset>
		<legend>Sign in using OpenID</legend>
		<div id="openid_choice">
			<p>Please select your account provider:</p>
			<select name="identifier">
				<option value="https://www.google.com/accounts/o8/id">Google</option>
				<option value="http://yahoo.com/">Yahoo</option>
				<option value="http://steamcommunity.com/openid/">Steam</option>
			</select>
		</div>
		<p>
			<input type="submit" value="Sign In"/>
		</p>
	</fieldset>
	</form>

</div>
</body>
</html>