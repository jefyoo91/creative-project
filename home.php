<!DOCTYPE html>
<?php session_start(); ?>
<html>
<head><title>Your Home Screen</title>
<style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	div.public{
		text-align: left;
		margin: 5px auto;
		border: 1px solid black;
	}
	</style></head>
<body>
	<?php if (!$_SESSION['openid.identity']){
	header('Location: login.php');
	exit;
}
else { ?>
	<div class='box'>
		Welcome <br/>
		<b>Public Recipes:</b>
		<?php
			require 'database.php';
			
			$userid = $_SESSION['openid.identity'];
			$public_data = $mysqli->prepare("select id, title, category from public order by id DESC");
			if(!$public_data){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
	 
			$public_data->execute();
	 
			$public_data->bind_result($pub_id, $pub_title, $pub_category);
	 
			while($public_data->fetch()){
				$output= sprintf("<div class='public'><a href='view_public.php?public_id=$pub_id'>public ID: %s \n<ul><li>Title: %s</li>\n <li>Category: %s</li>\n </ul>\n</a></div>",
					htmlspecialchars($pub_id),
					htmlspecialchars($pub_title),
					htmlspecialchars($pub_category)
				);
				echo($output);
			}
	 
			$public_data->close();
			echo("<br/><b>Private Recipes:</b><br/>");
			$private_data = $mysqli->prepare("select id, title, category from private_rec where user = ?");
			if(!$private_data){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
	 
			$private_data->execute();
	 
			$private_data->bind_result($priv_id, $priv_title, $priv_category);
	 
			while($private_data->fetch()){
				$output= sprintf("<div class='private'><a href='view_private.php?private_id=$priv_id'>private ID: %s \n<ul><li>Title: %s</li>\n <li>Category: %s</li>\n </ul>\n</a></div>",
					htmlspecialchars($priv_id),
					htmlspecialchars($priv_title),
					htmlspecialchars($priv_category)
				);
				echo($output);
			}
		?>
		<b>Logout:</b>
			<form action = 'logout_process.php'>
				<input type='submit' value='Logout' class='btn' />
			</form><br>
		<b>Submit a recipe:</b>
			<form action = 'submit.php' method='post'>
				<input type='submit' value='Submit a recipe' class='btn' />
			</form><br>
		<?php }?>
	</div>
</body>
</html>
