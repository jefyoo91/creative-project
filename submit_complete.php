<!DOCTYPE html>
<html>
<head><title>Submission Complete</title><style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	</style></head>
<body>
	<div class="box">
		You did it. Congratulations. Your post was submitted.
		<form action='home.php'>
			<input type = 'submit' value = 'Return to Home Page' class = 'btn'/>
		</form>
	</div>
</body>
</html>