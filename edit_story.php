<!DOCTYPE html>
<?php session_start();
if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	exit;
}
?>
<html>
<head><title>Edit Story</title><style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	div.comment{
		text-align: left;
		margin: 5px auto;
		border: 1px solid black;
	}
	div.story{
		text-align: center;
		margin: 20px auto;
		border: 1px solid black;
	}
	</style></head>
<body>
	<div class='box'>
	<?php
		require 'database.php';
		
		$story_id = htmlspecialchars($_POST['story_id']);
		
		$story_data = $mysqli->prepare("select story_id, title, link, content, poster_id, category, votes from stories where story_id=?");
		if(!$story_data){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}
		
		$story_data->bind_param('i', $story_id);
			
		$story_data->execute();
		$story_data->bind_result($story_id, $title, $link, $content, $poster_id, $category, $votes);
		while($story_data->fetch()){
			printf("\tTitle: %s \tVotes: %s \tCategory: %s\n ",
				htmlspecialchars( $title ),
				htmlspecialchars( $votes ),
				htmlspecialchars( $category )
			);
			echo "<div class = 'story'>";
			if(!empty($link)){
				printf("<a href='%s'>\t<li>%s</li>\n</a>",
					htmlspecialchars($link),
					htmlspecialchars($link)
				);
			}
			else {echo "No Link";}
			echo "</div>";
			echo "<div class = 'story'>";
			print(htmlspecialchars($content) . "\n");
			echo "</div>";
		}
			
		$story_data->close();
	?>
	</div>
	<div class='box'>
		Edit a story:
		<form action = 'edit_story_process.php' method = 'post'>
			Title: <input type = 'text' name = 'title' size = '90' placeholder = 'Edit your title here.'/><br>
			Link: <input type = 'text' name = 'link' size = '90' placeholder = 'Edit your link here.'/><br>
			Content: <textarea name = 'content' cols = "60" rows = "20" placeholder = "Edit your content here."></textarea> <br>
			Category: <input type = 'radio' name = 'category' value='Link'/> Link 
			<input type = 'radio' name = 'category' value = 'Commentary'/> Commentary <br>
			<input type = 'submit' value = "Edit Post" class = 'btn'/>
			<input type = 'hidden' name = 'token' value="<?php echo $_SESSION['token'];?>" />
			<input type = 'hidden' name = 'story_id' value="<?php echo $_POST['story_id'];?>" />
		</form>
		<form action = 'delete_story.php' method = 'post'>
			<input type = 'hidden' name = 'token' value="<?php echo $_SESSION['token'];?>" />
			<input type = 'hidden' name = 'story_id' value="<?php echo $_POST['story_id'];?>" />
			<input type = 'submit' value = "Delete" class = 'btn'/>
		</form>
		<form action = 'user.php'>
			<input type = 'submit' value = "Back to User Page" class = 'btn'/>
		</form>
	</div>
</body>
</html>