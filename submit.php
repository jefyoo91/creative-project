<!DOCTYPE html>
<?php session_start();?>
<html>
<head><title>Submit a Story</title><style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	</style></head>
<body>
	<div class='box'>
		Submit a story:
		<form action = 'submit_process.php' method = 'post'>
			Title: <input type = 'text' name = 'title' size = '90' placeholder = 'Write your title here.'/><br>
			Link: <input type = 'text' name = 'link' size = '90' placeholder = 'Paste your link here.'/><br>
			Content: <textarea name = 'content' cols = "60" rows = "20" placeholder = "Write your content here."></textarea> <br>
			Category: <input type = 'radio' name = 'category' value='Link'/> Link 
			<input type = 'radio' name = 'category' value = 'Commentary'/> Commentary <br>
			<input type = 'submit' value = "Submit Post" class = 'btn'/>
			<input type = 'hidden' name = 'token' value="<?php echo $_SESSION['token'];?>" />
		</form>
		<form action = 'home.php'>
			<input type = 'submit' value = "Back to Home Page" class = 'btn'/>
		</form>
	</div>
</body>
</html>