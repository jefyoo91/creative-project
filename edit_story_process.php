<?php
session_start();

//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	exit;
}

require 'database.php';

//Checking CSRF token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

//Variable santization
$story_id = $_POST['story_id'];

$stmt2 = $mysqli->prepare("select poster_id from stories where story_id = ?");
if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
$stmt2 -> bind_param('i', $story_id);
$stmt2 -> execute();
$stmt2 -> bind_result($user_id);

while($stmt2->fetch()){
if($user_id != trim($_SESSION['user_id'])){
	header("Location: home.php");
	exit;
}
}
$stmt2 ->close();

$story_data = $mysqli->prepare("select title, link, content, category, votes from stories where story_id=?");
	if(!$story_data){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
			
	$story_data->bind_param('i', $story_id);
			
	$story_data->execute();
	 
	$story_data->bind_result($title, $link, $content, $category, $votes);

if($_POST['title']!=''){
$title = $mysqli -> real_escape_string($_POST['title']);
}
if($_POST['content']!=''){
$content = $mysqli -> real_escape_string($_POST['content']);
}
if($_POST['category']!=''){
$category = $mysqli -> real_escape_string($_POST['category']);
}
if($_POST['link']!=''){
$link = $mysqli -> real_escape_string($_POST['link']);
}
$story_data->close();
//Checking for duplicate title
$stmt2 = $mysqli->prepare("SELECT count(*) FROM stories WHERE title = ?");
if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

$stmt2->bind_param('s', $title);
$stmt2->execute();

$stmt2->bind_result($cnt);
$stmt2->fetch();
$stmt2->close();

//Insertion
if ($cnt <= 1){
	$stmt = $mysqli->prepare("UPDATE stories SET title =? , content =? , category =? , link =? WHERE story_id =?");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('ssssi', $title, $content, $category, $link, $story_id);
	$stmt -> execute();
	$stmt -> close();

	header("Location: user.php");
	exit;
}
?>