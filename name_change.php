<?php
session_start();

//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	exit;
}
elseif (!isset($_POST['username'])){
	header("Location: account_change.php");
	exit;
}

require 'database.php';

//Checking CSRF token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

//Variable santization
$user_id = $_SESSION['user_id'];

$username = $mysqli -> real_escape_string($_POST['username']);

$stmt2 = $mysqli->prepare("SELECT count(*) FROM users WHERE username = ?");

$stmt2->bind_param('s', $username);
$stmt2->execute();

$stmt2->bind_result($cnt);
$stmt2->fetch();
$stmt2->close();


if($cnt == 0){
	$stmt = $mysqli->prepare("UPDATE users SET username =? WHERE id =?");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('si', $username, $user_id);
	$stmt -> execute();
	$stmt -> close();
	$_SESSION['username'] = $username;
	header("Location: user.php");
	exit;
	}
?>