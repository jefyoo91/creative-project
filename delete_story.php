<?php
session_start();

//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	exit;
}

require 'database.php';

//Checking CSRF token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

//Variable santization
$story_id = $_POST['story_id'];

//Deletion
	$stmt = $mysqli->prepare("DELETE FROM stories WHERE story_id =?");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('i', $story_id);
	$stmt -> execute();
	$stmt -> close();

	header("Location: user.php");
	exit;

?>