<?php session_start(); 
if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	exit;
}
?>
<!DOCTYPE html>
<html>
<head><title>Viewing Private Recipe</title><style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	div.comment{
		text-align: left;
		margin: 5px auto;
		border: 1px solid black;
	}
	div.story{
		text-align: center;
		margin: 20px auto;
		border: 1px solid black;
	}
	div.commactual{
		text-align: left;
		margin: 5px 5px;
		border: 1px solid black;
	}
	</style></head>
<body>
	<div class='box'>
		<?php
			require 'database.php';
			
			$pub_id = htmlspecialchars($_GET['private_id']);
			
			$story_data = $mysqli->prepare("select title, category, ingred, steps, img, video, pub_key, descrip, user, public.title from private_rec LEFT JOIN public ON public.id = private.pub_key and private.id = ?");
			if(!$story_data){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			
			$story_data->bind_param('i', $pub_id);
				
			$story_data->execute();
	 
			$story_data->bind_result($title, $category, $ingred, $steps, $img, $video, $pubkey, $descrip, $user);

			while($story_data->fetch()){
				printf("Title: %s \n Description: %s \n Category: %s\n ",
					htmlspecialchars( $title ),
					htmlspecialchars( $descrip ),
					htmlspecialchars( $category )
				);
				echo "<div class = 'story'>";
				echo("<br/><b>Original Recipe:</b><br/>");
				printf("<a href='%s'>\t%s\n</a>",
					htmlspecialchars($link),
					htmlspecialchars($link)
				);
				echo "</div>";
				echo "<div class = 'story'>";
				printf(htmlspecialchar($ingred) . "\n");
				echo "</div>";
				echo "<div class = 'story'>";
				print(htmlspecialchars($steps) . "\n");
				echo "</div>";
			}
			
			$story_data->close();

		?>
		</div>
	</body>
</html>