<?php
session_start();

//Checking to see if the user is signed in and has submitted information for all fields.
if(!isset($_SESSION['openid.identifier'])){
	header("Location: login.php");
	exit;
}

require 'database.php';

//Checking CSRF token
if($_SESSION['token'] !== $_POST['token']){
	die("Request forgery detected");
}

//Variable santization
$video_regex = "/youtube.com[\S]*([\w_-]{11}$)/";
$video_match = "";
$image_regex = "/.jpg$|.png$|.gif$/";

$title = htmlspecialchars($_POST['title']);
$description = htmlspecialchars($_POST['description']);
$ingredients = htmlspecialchars($_POST['ingredients']);
$steps = htmlspecialchars($_POST['steps']);
$poster_id = htmlspecialchars($_SESSION['openid.identifier']);
$category = htmlspecialchars($_POST['category']);
if(!empty(htmlspecialchars($_POST['video']))){
	if(preg_match($video_regex, htmlspecialchars($_POST['video']), $video_match)){
		$video = $video_match[1];
	}else{
		alert("Only youtube.com links supported!\nRecipe will be added without video!");
		$video = null;
	}
}else $video = null;
if(preg_match($image_regex, htmlspecialchars($_POST['image']))){
	
	
}else{
	alert("Only .png, .jpg, and .gif extensions supported!\nRecipe will be added without image!");
	$image = null;
}

//Checking for duplicate title
$stmt2 = $mysqli->prepare("SELECT count(*) FROM public WHERE title = ?");
if(!$stmt2){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

$stmt2->bind_param('s', $title);
$stmt2->execute();

$stmt2->bind_result($cnt);
$stmt2->fetch();
$stmt2->close();

//Insertion
if ($cnt == 0){
	$stmt = $mysqli->prepare("INSERT INTO public (title, ingred, steps, user, video, descrip, category) VALUES (?, ?, ?, ?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt -> bind_param('sssssss', $title, $ingredients, $steps, $poster_id, $video, $description, $category);
	$stmt -> execute();
	$stmt -> close();

	$stmt4 = $mysqli->prepare("SELECT id from public where title = ?");
	if(!$stmt4){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
	$stmt4 -> bind_param('s', $title);
	$stmt4 -> execute();
	$stmt4 -> bind_result($id);
	$stmt4 -> fetch();
	$stmt4 -> close();
	
	$filename = basename($_FILES['image']['name']);
	if(!empty($filename)){
		if(preg_match($image_regex, $filename)){
			$full_path = sprintf("/Module8/images/%s/%s",$id,$filename);
			if( move_uploaded_file($_FILES['image']['tmp_name'], $full_path) ){
			}else{
				alert("Image Upload Failed!\nRecipe will be added without image!");
				exit;
			}
			$imagesql = $mysqli->prepare("UPDATE public SET image = ? WHERE id = ?");
			if(!$imagesql){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$imagesql -> bind_param('ss', $full_path, $id);
			$imagesql -> execute();
			$imagesql -> close();
		}else{
			alert("Only .png, .jpg, and .gif extensions supported!\nRecipe will be added without image!");
			$image = null;
		}
	}
	header("Location: submit_complete.php");
	exit;
}
?>