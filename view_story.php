<?php session_start(); 
if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	exit;
}
?>
<!DOCTYPE html>
<html>
<head><title>Viewing Story</title><style type="text/css">
	div.box{
		text-align: center;
		width: 500px;
		margin: 100px auto;
		padding: 25px;
		border: 1px solid black;
	}
	div.comment{
		text-align: left;
		margin: 5px auto;
		border: 1px solid black;
	}
	div.story{
		text-align: center;
		margin: 20px auto;
		border: 1px solid black;
	}
	div.commactual{
		text-align: left;
		margin: 5px 5px;
		border: 1px solid black;
	}
	</style></head>
<body>
	<div class='box'>
		<?php
			require 'database.php';
			
			$story_id = htmlspecialchars($_GET['story_id']);
			
			$story_data = $mysqli->prepare("select story_id, title, link, content, poster_id, category, votes from stories where story_id=?");
			if(!$story_data){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			
			$story_data->bind_param('i', $story_id);
				
			$story_data->execute();
	 
			$story_data->bind_result($story_id, $title, $link, $content, $poster_id, $category, $votes);

			while($story_data->fetch()){
				printf("Title: %s \n Votes: %s \n Category: %s\n ",
					htmlspecialchars( $title ),
					htmlspecialchars( $votes ),
					htmlspecialchars( $category )
				);
				echo "<div class = 'story'>";
				if(!empty($link)){
					printf("<a href='%s'>\t%s\n</a>",
						htmlspecialchars($link),
						htmlspecialchars($link)
					);
				}
				else {echo "No Link";}
				echo "</div>";
				echo "<div class = 'story'>";
				print(htmlspecialchars($content) . "\n");
				echo "</div>";
			}
			
			$story_data->close();

			$comment_data = $mysqli->prepare("select comment_id, story_id, content, users.username, votes, poster_id from comments join users on (comments.poster_id = users.id) where story_id=? order by votes DESC");
			if(!$comment_data){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}
			$comment_data->bind_param('i', $story_id);
			$comment_data->execute();
			$comment_data->bind_result($comment_id, $story_id, $content, $username, $votes, $poster_id);

			while($comment_data->fetch()){
				printf("<div class='comment'>Poster: %s \n Votes: %s \n",
					htmlspecialchars( $username),
					htmlspecialchars( $votes )
				);
				printf("<div class='commactual'> %s </div>", htmlspecialchars($content));
				printf("<form action = 'vote_process.php' method = 'post'>
					<input type = 'submit' value = 'Vote for This Comment!' class = 'btn' />
					<input type = 'hidden' name = 'content_id' value = '%s' />
					<input type = 'hidden' name = 'user_id' value = '%s' />
					<input type = 'hidden' name = 'content_type' value = 'Comment' />
					<input type = 'hidden' name = 'origin' value = 'view_story.php' />
					<input type = 'hidden' name = 'token' value = '%s' />
				</form>", $comment_id, $_SESSION['user_id'], $_SESSION['token']);
				echo "</div>";
			}
			$comment_data->close();
		?>
		<form action = 'vote_process.php' method = 'post'>
			<input type = 'submit' value = 'Vote for This Story!' class = 'btn' />
			<input type = 'hidden' name = 'content_id' value = "<?php echo $_GET['story_id']; ?>" />
			<input type = 'hidden' name = 'user_id' value = "<?php echo $_SESSION['user_id']; ?>" />
			<input type = 'hidden' name = 'content_type' value = 'Story' />
			<input type = 'hidden' name = 'origin' value = 'view_story.php' />
			<input type = 'hidden' name = 'token' value = "<?php echo $_SESSION['token'];?>" />
		</form>
		<br><br>
		Submit a comment:
			<form action = 'submit_comment.php' method = 'post'>
				<textarea name = 'content' cols = "60" rows = "7" placeholder = "Write your content here."></textarea> <br>
				<input type = 'submit' value = "Submit Comment" class = 'btn'/>
				<input type = 'hidden' name = 'token' value="<?php echo $_SESSION['token'];?>" />
				<input type = 'hidden' name = 'story_id' value="<?php echo $_GET['story_id'];?>" />
			</form>
			<form action = 'home.php'>
				<input type = 'submit' value = "Back to Home Page" class = 'btn'/>
			</form>
		</div>
	</body>
</html>